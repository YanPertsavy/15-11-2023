﻿#include <iostream>

using namespace std;

void randomArray(int(&arr)[20], int z);

bool IsPrime(int i);

int SumMassive(int arr[], int);

int main()
{
    const int z = 20;
    int arr[z]{0};
    randomArray(arr, z);
    cout << SumMassive(arr, z);
}
void randomArray(int(&arr)[20], int z)
{
    srand(time(NULL));
    for (size_t i = 0; i < z; i++)
    {
        arr[i] = rand() % 100;
        cout << arr[i] << " ";
    }
}
bool IsPrime(int h)
{
    for (size_t i = 2; i < sqrt(h); i++)
    {
        if (h % i == 0)
        {
            return false;
        }

    }
    return true;
}
int SumMassive(int arr[], int z)
{
    int sum = 0;
    for (size_t i = 0; i < z; i++)
    {
        if (IsPrime(arr[i]) == true)
        {
            sum += arr[i];
        }
    }
    return sum;
}